#!/bin/sh

export ENV=/home/diegofcs/.mkshrc

# nnn Plugins

NNN_PLUG_DIR='r:renamer'
NNN_PLUG_FILE='z:-!ffplay -x 850 -y 650 $nnn*'
NNN_PLUG_VARIOS='p:di-nuke'
NNN_PLUG_VIS='e:-!vis $nnn*'
NNN_PLUG="$NNN_PLUG_DIR;$NNN_PLUG_FILE;$NNN_PLUG_VARIOS;$NNN_PLUG_VIS"
export NNN_PLUG

# nnn options 
export NNN_ARCHIVE='\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$'
export NNN_OPENER=/home/diegofcs/.config/nnn/plugins/di-nuke
export NNN_USE_EDITOR=1                   
export NNN_OPTS="ceEuo"
export NNN_FIFO=/tmp/nnn.fifo 
export PAGER="less"               
export EDITOR="vis"              
export VISUAL="$PAGER"               
export IMAGEVIEWER="display" 
export FILE="nnn"
export BROWSER='qutebrowser'
export TERM="st-256color"
export LANG=es_CO.UTF-8
export MENU="dmenu"
export sel=${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection
export READER="zathura"

#nnn bookmarks
export NNN_BMS='t:~/Documentos/Trabajo_oficina/;i:~/Documentos/Trabajo_independiente;v:~/Documentos/Variados;d:~/Descargas;s:~/Scripts;w:~/Imagenes/Wallpapers;c:~/Imagenes/Screenshots'

#nnn colors
BLK="0B" CHR="0B" DIR="04" EXE="06" REG="00" HARDLINK="06" SYMLINK="06" MISSING="00" ORPHAN="09" FIFO="06" SOCK="0B" OTHER="06"
export NNN_FCOLORS="$BLK$CHR$DIR$EXE$REG$HARDLINK$SYMLINK$MISSING$ORPHAN$FIFO$SOCK$OTHER"

