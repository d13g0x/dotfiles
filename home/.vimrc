" 
" ██╗   ██╗██╗███╗   ███╗██████╗  ██████╗
" ██║   ██║██║████╗ ████║██╔══██╗██╔════╝
" ██║   ██║██║██╔████╔██║██████╔╝██║     
" ╚██╗ ██╔╝██║██║╚██╔╝██║██╔══██╗██║     
"  ╚████╔╝ ██║██║ ╚═╝ ██║██║  ██║╚██████╗
"   ╚═══╝  ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝ ╚═════

let mapleader =","
set nocompatible
syntax on 
set mouse=a  
set clipboard=unnamedplus 
set number relativenumber
set encoding=utf-8
colorscheme base16-grayscale-dark
set showcmd
highlight clear LineNr
set nocursorcolumn

" Trabajar con tabulador
    set shiftwidth=4
    set tabstop=4
    set expandtab

"Configurar teclas
    nnoremap <F2> :setlocal spell! spelllang=es<CR>

"Vim plugins
call plug#begin()
    Plug 'lilydjwg/colorizer'
call plug#end()

"LaTeX

"Configuraciones básicas vim-latex
filetype plugin on
filetype indent on
let g:tex_flavor='latex'

" Compiladores
map <leader>c :w! \| !pdflatex <c-r>%<CR>
map <leader>l :w! \| !lualatex <c-r>%<CR>

" Mostrar el pdf final
map <leader>z :!zathura <c-r>%<backspace><backspace><backspace>pdf&<CR><CR>

" Plantilas
map <leader>pln :-1read $HOME/.plantillas_latex/normal.tex<CR>
map <leader>plm :-1read $HOME/.plantillas_latex/membrete.tex<CR>


