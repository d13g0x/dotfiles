--- Cargar el módulo vis estandar
require('vis')
require('plugins/filetype')
require('plugins/vis-vim-compatible')

--- Plugins
require('plugins/vis-filetype-settings')
require('plugins/vis-vim-compatible')

--- Opciones de configuración global
vis.events.subscribe(vis.events.INIT, function()
    vis:command('set theme base16-grayscale-dark')
    vis:command('set autoindent')
    vis:command('set ignorecase')
    vis:command('set tabwidth 4')
    vis:command('set expandtab')
end)

--- Opciones de scripts
settings = {
	filetype = settings
}
--- Opciones de configuracion por ventana
vis.events.subscribe(vis.events.WIN_OPEN, function(win)
    vis:command('set relativenumbers')

    if vis.win.syntax == 'bash' then
        vis:command('set colorcolumn 110')
end

--- Abrir plantillas LATEX
vis:map(vis.modes.NORMAL, "<F4>", ':e $HOME/.plantillas_latex/membrete.tex<Enter>')
vis:map(vis.modes.NORMAL, "<F5>", ':e $HOME/.plantillas_latex/normal.tex<Enter>')

--- Abrir explorador de vis
vis:map(vis.modes.NORMAL, "<F1>",
	function()
		vis:command(':e $HOME')
end)

--- Compilar archivos de LATEX
vis:map(vis.modes.NORMAL, "<F2>",
	function()
		vis:command('!lualatex $vis_filename')
end)

--- Abrir PDF
vis:map(vis.modes.NORMAL, "<F3>",
	function()
		vis:command('!vis-open -p :e *.pdf | xargs zathura &')
	end)
end)
